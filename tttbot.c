/* tttbot - tic-tac-toe simulator
 *
 * plays any number of tic-tac-toe games against itself,
 * reporting the results. All games should end in ties.
 */

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define FLAG_WIN 1
#define FLAG_BLOCK -1
#define DEBUG 0
#define P(x) player[(x)+1]

static int board[10];
static char player[] = {'O',' ','X'};
    
void debug(char *fmt, ...) {
    if (!DEBUG)
        return;
    va_list args;

    fflush(stdout);
    if (getprogname() != NULL)
        fprintf(stderr, "%s: ", getprogname());
    va_start(args,fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);

    if(fmt[0] != '\0' && fmt[strlen(fmt)-1] == ':')
        fprintf(stderr, " %s", strerror(errno));
}

void init_board(void) {
    for (int i=0; i<10; i++)
        board[i] = 0;
}

void print_board(void) {
    printf("---------\n");
    for (int i=1; i<10; i++) {
        printf("%c", P(board[i]));
        if (i%3) 
            printf(" | ");
        else
            printf("\n");
    }
    printf("---------\n");
}

void report_winner(int winner) {
    if (winner) {
        printf("%c was Winner Winner Chicken Dinner!!!\n", P(winner));
        print_board();
    } else {
        debug("Game ended in a tie\n");
    }
}

/* finds either a winning or blocking play, depending on the value of
 * the flag winorblock.
 *
 * @return the player who made the move
 */
int look_for_winorblock(int pl, int winorblock) {
    int rows[8][3] = {{1,2,3}, {4,5,6}, {7,8,9}, {1,4,7},
                      {2,5,8}, {3,6,9}, {1,5,9}, {7,5,3}};
    
    for (int i=0; i<8; i++) {
        int t = board[rows[i][0]] + board[rows[i][1]] + board[rows[i][2]];
        debug("Row %d %d %d : total %d\n", rows[i][0], rows[i][1], rows[i][2], t);
        if (t==2*pl*winorblock) {
            debug("Winning or blocking (%d) in row %d\n", winorblock, i);
            for (int j=0; j<3; j++) {
                debug("Try winning or blocking (%d) in i=%d, j=%d, cell=%d\n", winorblock, i, j, rows[i][j]);
                if (board[rows[i][j]] == 0) {
                    debug("Winning or blocking (%d) in cell %d\n", winorblock, rows[i][j]);
                    board[rows[i][j]] = pl;
                    return pl;
                }
            }
        }
    }
    return 0;
}

int look_for_play(int pl, int turn) {
    int rows[8][3] = {{1,2,3}, {4,5,6}, {7,8,9}, {1,4,7},
                      {2,5,8}, {3,6,9}, {1,5,9}, {7,5,3}};
    int start;
    int end;
    
    if (turn == 0) {                  /* X's first turn */
        board[random() % 9 + 1] = pl;
        return pl;
    } else if (turn == 1) {           /* O's first turn */
        if (board[5]==0) {
            board[5] = pl;
            return pl;
        } else
            board[1] = pl;
    } else {
        for (int i=0; i<8; i++) {
            int t = board[rows[i][0]] + board[rows[i][1]] + board[rows[i][2]];
            debug("Row %d %d %d : total %d\n", rows[i][0], rows[i][1], rows[i][2], t);
            if (t==1*pl) {
                long rnd = random();
                start = rnd % 3;
                end = start;
                debug("rnd=%ld, start=%d, end=%d\n", rnd, start, end);
                start++;
                debug("start=%d, end=%d\n", start, end);
                while (1) {
                    if (start > 2)
                        start = 0;
                    if (start == end)
                        break;
                    debug("Try playing in i=%d, start=%d, end=%d, cell=%d\n",  i, start, end, rows[i][start]);

                    if (board[rows[i][start]] == 0) {
                        debug("Playing in cell %d\n", rows[i][start]);
                        board[rows[i][start]] = pl;
                        return pl;
                    }
                    start++;
                }
            }
        }
        start = random() % 9 + 1;
        end = start;
        start++;
        while (1) {
            if (start > 9)
                start = 1;
            if (start == end)
                break;
            if (board[start] == 0) {
                board[start] = pl;
                return pl;
            }
            start++;
        }
    }
    return 0;
}

int take_a_turn(int pl, int turn) {
    int r = 0;

    r = look_for_winorblock(pl,FLAG_WIN);
    if (r)
        return r;
    r = look_for_winorblock(pl,FLAG_BLOCK);
    if (r)
        return 0;
    r = look_for_play(pl, turn);
    return 0;
}

int play_a_game(void) {
    int winner;

    init_board();

    for (int i=0; i<9; i++) {
        winner = take_a_turn((i%2 ? -1 : 1), i);
        if (winner)
            break;
    }
    report_winner(winner);
    return winner;
    /* return winner or 0 if a tie */
}

int main(int argc, char *argv[]) {
    int wins[] = {0,0,0}; /* O tie X */
    int games = 0;
    int r;
    int n = 10000;
    
    printf("I am tttbot!\n");
    srandom(time(NULL));

    if (argc > 1)
        n = (int)strtol(argv[1], NULL, 10);
    if (!n) {
        printf("Usage: %s <number_of_games>\n", argv[0]);
        return 1;
    }
    
    for (int i=0; i<n; i++) {
        games++;
        r = play_a_game();
        wins[r+1]++;
    }

    printf("Games: %d, X wins: %d, O wins: %d, ties: %d\n", games, wins[2], wins[0], wins[1]);
    return 0;
}
