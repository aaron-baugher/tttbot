
CC= cc
CFLAGS=	-g -Wall -std=c99 -pedantic

all: tttbot

tttbot: tttbot.c
	$(CC) $(CFLAGS) -o tttbot tttbot.c

clean:
	rm tttbot

install: tttbot
	install -m 755 tttbot $(HOME)/bin

